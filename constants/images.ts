import profile from "../assets/images/profile.png";
import thumbnail from "../assets/images/thumbnail.png";
import cards from "../assets/images/cards.png";
import path from "../assets/images/path.png";
import logo from "../assets/images/logo.png";
import logoSmall from "../assets/images/logo-small.png";
import empty from "../assets/images/empty.png";

interface Images {
    profile: any,
    thumbnail: any,
    cards: any,
    path: any,
    logo: any,
    logoSmall: any,
    empty: any
}

const images: Images = {
    profile,
    thumbnail,
    cards,
    path,
    logo,
    logoSmall,
    empty
}

export default images;
