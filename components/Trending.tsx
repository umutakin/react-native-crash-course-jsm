import {
    View,
    Text,
    FlatList,
    ViewStyle,
    TextStyle,
    ImageStyle,
    TouchableOpacity,
    ImageBackground,
    Image,
    ViewToken,
} from "react-native";
import * as Animatable from "react-native-animatable";
import React, { useState } from "react";
import { Models } from "react-native-appwrite";
import icons from "@/constants/icons";
import { Video, ResizeMode } from "expo-av";

interface TrendingProps {
    posts: Models.Document[];
}

interface TrendingItemProps {
    activeItem: Models.Document;
    item: Models.Document;
}

const zoomIn: Animatable.CustomAnimation<TextStyle & ViewStyle & ImageStyle> = {
    0: {
        transform: [{ scale: 0.9 }],
    },
    1: {
        transform: [{ scale: 1.1 }],
    },
};

const zoomOut: Animatable.CustomAnimation<TextStyle & ViewStyle & ImageStyle> =
    {
        0: {
            transform: [{ scale: 1.1 }],
        },
        1: {
            transform: [{ scale: 0.9 }],
        },
    };

const TrendingItem: React.FC<TrendingItemProps> = ({ activeItem, item }) => {
    const [play, setPlay] = useState(false);
    const [status, setStatus] = useState({});

    return (
        <Animatable.View
            className="mr-5"
            animation={activeItem.$id === item.$id ? zoomIn : zoomOut}
            duration={250}
        >
            {play ? (
                <Video
                    source={{ uri: item.video }}
                    className="w-52 h-72 rounded-[35px] mt-3 bg-white/10"
                    resizeMode={ResizeMode.CONTAIN}
                    useNativeControls
                    shouldPlay
                    onPlaybackStatusUpdate={(status) => {
                        if (status.isLoaded) {
                            if (status.didJustFinish) {
                                setPlay(false);
                            }
                        }
                    }}
                />
            ) : (
                <TouchableOpacity
                    className="relative justify-center items-center"
                    activeOpacity={0.7}
                    onPress={() => setPlay(true)}
                >
                    <ImageBackground
                        source={{ uri: item.thumbnail }}
                        className="w-52 h-72 rounded-[35px] my-5 overflow-hidden shadow-lg shadow-black/40"
                        resizeMode="cover"
                    />
                    <Image
                        source={icons.play}
                        className="w-12 h-12 absolute"
                        resizeMode="contain"
                    />
                </TouchableOpacity>
            )}
        </Animatable.View>
    );
};

const Trending: React.FC<TrendingProps> = ({ posts }) => {
    const [activeItem, setActiveItem] = useState(posts[1]);

    const viewableItemsChanged = ({
        viewableItems,
    }: {
        viewableItems: Array<ViewToken>;
    }) => {
        const activePost = posts.find(
            (post) => post.$id === viewableItems[0].item.$id
        );
        if (activePost) {
            setActiveItem(activePost);
        }
    };

    return (
        <FlatList
            data={posts}
            keyExtractor={(item) => item.$id}
            renderItem={({ item }) => (
                <TrendingItem activeItem={activeItem} item={item} />
            )}
            onViewableItemsChanged={({ viewableItems }) =>
                viewableItemsChanged({ viewableItems })
            }
            viewabilityConfig={{ itemVisiblePercentThreshold: 70 }}
            contentOffset={{ x: 170, y: 0 }}
            horizontal
        />
    );
};

export default Trending;
