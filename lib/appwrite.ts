import {
    Client,
    Account,
    ID,
    Avatars,
    Databases,
    Query,
    Models,
    Storage,
    ImageGravity,
} from "react-native-appwrite";

export const config = {
    endpoint: "https://cloud.appwrite.io/v1",
    platform: "com.umutakin.aora",
    projectId: "66732149000bb1a56130",
    databaseId: "667322c200199c78b220",
    userCollectionId: "667322f2002f91f6c760",
    videoCollectionId: "66732326002fe5bd267f",
    storageId: "667324c9003ad43c6265",
};

const { endpoint, platform, projectId, databaseId, userCollectionId, videoCollectionId, storageId } = config;

// Init your React Native SDK
const client = new Client();

client
    .setEndpoint(config.endpoint)
    .setProject(config.projectId)
    .setPlatform(config.platform);

const account = new Account(client);
const avatars = new Avatars(client);
const databases = new Databases(client);
const storage = new Storage(client);

export const createUser = async (
    email: string,
    password: string,
    username: string
) => {
    try {
        const newAccount = await account.create(
            ID.unique(),
            email,
            password,
            username
        );

        if (!newAccount) {
            throw Error;
        }

        const avatarUrl = avatars.getInitials(username);

        await signIn(email, password);

        const newUser = await databases.createDocument(
            databaseId,
            userCollectionId,
            ID.unique(),
            {
                accountId: newAccount.$id,
                email,
                username,
                avatar: avatarUrl,
            }
        );

        return newUser;
    } catch (error: any) {
        console.log(error);
        throw new Error(error);
    }
};

export async function signIn(email: string, password: string) {
    try {
        const session = await account.createEmailPasswordSession(email, password);
    
        return session;
      } catch (error: any) {
        throw new Error(error);
      }
    
}

export async function getCurrentUser(): Promise<Models.Document | null> {
    try {
        const currentAccount = await account.get();
        if (!currentAccount) throw Error;
        const currentUser = await databases.listDocuments(
            databaseId,
            userCollectionId,
            [Query.equal("accountId", currentAccount.$id)]
        );
        if (!currentUser) throw Error;

        return currentUser.documents[0];
    } catch (error) {
        console.log(error);
        return null;
    }
}

export async function getAllPosts(): Promise<Models.Document[]> {
    try {
        const posts = await databases.listDocuments(
            databaseId,
            videoCollectionId,
            [Query.orderDesc('$createdAt')]
        )

        return posts.documents;
    } catch (error: any) {
        throw new Error(error);
    }
}

export async function getLatestPosts(): Promise<Models.Document[]> {
    try {
        const posts = await databases.listDocuments(
            databaseId,
            videoCollectionId,
            [Query.orderDesc('$createdAt'), Query.limit(7)]
        )

        return posts.documents;
    } catch (error: any) {
        throw new Error(error);
    }
}

export async function searchPosts(query: string): Promise<Models.Document[]> {
    try {
        const posts = await databases.listDocuments(
            databaseId,
            videoCollectionId,
            [Query.search('title', query)]
        )

        return posts.documents;
    } catch (error: any) {
        throw new Error(error);
    }
}

export async function getUserPosts(userId: any): Promise<Models.Document[]> {
    try {
        const posts = await databases.listDocuments(
            databaseId,
            videoCollectionId,
            [Query.equal("creator", userId)]
        )

        return posts.documents;
    } catch (error: any) {
        throw new Error(error);
    }
}

export const signOut = async () => {
    try {
        const session = await account.deleteSession("current");
        return session;
    } catch (error: any) {
        throw new Error(error);
    }
}

export const getFilePreview = async  (fileId: any, type: any) => {
    let fileUrl;

    try {
        if (type === "video") {
            fileUrl = storage.getFileView(storageId, fileId);
        } else if (type === "image") {
            fileUrl = storage.getFilePreview(storageId, fileId, 2000, 2000, ImageGravity.Top, 100)
        } else {
            throw new Error("Invalid file type");
        }

        if (!fileUrl) {
            throw Error();
        }

        return fileUrl;
    } catch (error: any) {
        throw new Error(error);
    }
}

export const uploadFile = async (file: any, type: string) => {

    if (!file) {
        return;
    }

    // const { mimeType, ...rest } = file;
    // const asset = { type: mimeType,  ...rest }
    const asset = {
        name: file.fileName,
        type: file.mimeType,
        size: file.fileSize,
        uri: file.uri
    }

    try {
        const uploadedFile = await storage.createFile(
            storageId,
            ID.unique(),
            asset
        )

        const fileUrl = await getFilePreview(uploadedFile.$id, type);

        return fileUrl;
    } catch (error: any) {
        throw new Error(error);
        
    }

} 

export const createVideo = async (form: any) => {
    try {
        const [thumbnailUrl, videoUrl] = await Promise.all([
            uploadFile(form.thumbnail, "image"),    
            uploadFile(form.video, "video"),

        ]);

        const newPost = await databases.createDocument(databaseId, videoCollectionId, ID.unique(), {
            title: form.title,
            thumbnail: thumbnailUrl,
            video: videoUrl, 
            prompt: form.prompt,
            creator: form.userId
        })

        return newPost;
    } catch (error: any) {
        throw new Error(error);
        
    }
}